# The creatiVision Emulator #

This is an emulator of the VTech Creativision, Funvision, Disk Smith Wizzard and Hanimex Rameses consoles.
It has been updated to also emulate the Laser 2001 and Salora Manager computers.

## Binaries 16.04.24 ##
* Now only Windows 32 bit provided

## What's New in 16.04.24 ##
* Salora Manager / Laser 2001 Disc Controller ROM dumped
* Salora Manager / Laser 2001 Floppy Disc Image emulation
* Bug fixes

Requirements
------------

Your operating system **MUST** support SDL 2.0.3.

This project is developed with CodeBlocks and SDL 2.0.3.

##Available options##

|Option  | Detail |
|:------:| ------ |
| **-b** | Load alternative BIOS rom. Default is BIOS/BIOSCV.ROM |
|        | By default the emulator will look in a BIOS subfolder for bioscv.rom |
| **-r** | Load alternative ROM binary. |
|        | Default is ROMS/CART.BIN |
|        | Any game renamed to CART.BIN in the ROMS subfolder will be loaded. |
|        | Use the -r option so select another. |
| **-g** | Load ROM in linear mode. Useful for developers. |
| **-m** | Magnify. 2=512x384(Default) 1x to 4x zoom |
|        | SDL enables scaling up to 4x windowed. |
| **-f** | Fullscreen 4:3 Aspect. Use the entire desktop area. |
| **-v** | VDP Debug mode enabled. Use F12 stop/step. F11 continue. |
|        | This option is for developers, and dumps the contents of VDP registers 0 through 7 and the Status Register. |
| **-t** | Trace debugger enabled. | 
|        | This enable EMUlibs built in 6502 debugger. Very useful for tracing through your own code. | 
| **-s** | Use Low Quality sound. Instead of 44100 Hz, use 22500 Hz. |
| **-l** | Load CAS files at double speed. CLOAD / CRUN. |
|        | This is currently unique to this emulator, in that it enables loading and loading of raw BASIC cassette files.  The double speed option should only be used with BASIC83. |
| **-c** | Set CLOAD/CSAVE filename. Default CSAVE.CAS | 
|        | The name of the cassette you wish to save to or load from. |
| **-p** | Set LLIST/LPRINT filename. Default PRINTER.TXT |
|        | An output file name for BASIC. Useful for getting straight ASCII text files. | 
| **-d** | Demo recording. Enable recording to AVI of the game / demo you are running. It is uncompressed, and will create huge files!. | 
| **-2** | Run in CSL Cartridge mode. Pretend to be a Salora Manager or Laser 2001 |
| **-3** | Run in Salora Manager or Laser 2001 mode. | 
| **-k** | Patch unverified TMS9929 addressing for Salora Manager or Laser 2001 |
| **-n** | Network player. 1 is server, 2 is client |
| **-i** | IP address or hostname of server |
| **-e** | Select floppy disc image. Default floppy.bin


## Emulator Keys ##

* ESC     Terminate emulation
* F2      Pause game play
* F3      Reset
* F4		Rewind cassette tape
* F5      Take BMP picture
* F7      Take snapshot
* F8      Load snapshot
* F10     Dump RAM and VRAM
* F11     VDP Debug
* F12	    VDP Debug

For keyboard programs, you can also use the letters and numbers of the keyboard. Note though, that same are not direct translations.

For normal games, on loading, you will observe a demonstration mode of the game. Pressing F3 (RESET to the creatiVision) will place you in game select mode. Use the fire buttons(CTRL/SHIFT) to select the game mode number you require, then press any alphabetic key on the lower row. Enjoy your game!
