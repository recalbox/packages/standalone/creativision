@echo off
setlocal enableextensions
for /f "tokens=*" %%a in (
	'gcc --version ^| find /i "gcc"'
) do (
	set PLATFORM=%%a
)

echo const char *PLATFORM="%PLATFORM%"; > platform.h
